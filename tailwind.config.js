const colors = require("tailwindcss/colors");

module.exports = {
  content: [],
  purge: {
    enabled: process.env.TAILWIND_MODE === "build",
    content: ["./src/**/*.{html,scss,ts}"],
  },
  theme: {
    extend: {
      fontFamily: {
        theme: ["yekan"],
      },
      colors: {
        primary: colors.blue,
        secondary: colors.gray,
        white: colors.white,
        danger: colors.red,
        success: colors.emerald,
        third: colors.yellow,
      },
    },
  },
  plugins: [],
};
