import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";

@Injectable({
    providedIn: 'root'
})

export class NotificationService {
    constructor(private snackBar: MatSnackBar) { }

    success(message: string) {
        this.snackBar.open(message, '', {
            panelClass: 'bg-success-800',
            duration: 3200
        });

    }

    error(message: string) {
        this.snackBar.open(message, '', {
            panelClass: 'bg-danger-800',
            duration: 3200
        });

    }
}