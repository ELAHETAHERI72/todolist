import { Injectable } from "@angular/core";
import { Subject } from "rxjs/internal/Subject";

@Injectable({
    providedIn: 'root'
})

export class UpdateTaskService {
    constructor() { }

    task = new Subject<any>();

    assingTaskValue(item: any) {
        this.task.next(item)
    }
}