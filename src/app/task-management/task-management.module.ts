import { NgModule } from '@angular/core';

import { TaskManagementRoutingModule } from './task-management-routing.module';
import { TaskManagementComponent } from './task-management.component';
import { SharedModule } from '../shared/shared.module';
import { ImplementTaskDialogComponent } from './implement-task-dialog/implement-task-dialog.component';

@NgModule({
  declarations: [
    TaskManagementComponent,
    ImplementTaskDialogComponent
  ],
  imports: [
    TaskManagementRoutingModule,
    SharedModule
  ]
})
export class TaskManagementModule { }
