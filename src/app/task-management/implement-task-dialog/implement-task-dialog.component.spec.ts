import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImplementTaskDialogComponent } from './implement-task-dialog.component';

describe('ImplementTaskDialogComponent', () => {
  let component: ImplementTaskDialogComponent;
  let fixture: ComponentFixture<ImplementTaskDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImplementTaskDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImplementTaskDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
