import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NotificationService } from 'src/app/shared/services/notificaton.service';
import { TodoDataService } from 'src/app/shared/services/todo-data.service';
import { UpdateTaskService } from 'src/app/shared/services/update-task.service';

@Component({
  selector: 'app-implement-task-dialog',
  templateUrl: './implement-task-dialog.component.html',
  styleUrls: ['./implement-task-dialog.component.scss']
})
export class ImplementTaskDialogComponent implements OnInit {

  taskForm!: FormGroup;

  ngOnInit(): void {
    this.initForm();
    this.setFormValue();

  }
  constructor(
    public dialogRef: MatDialogRef<ImplementTaskDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private todoDataService: TodoDataService,
    private notificationService: NotificationService,
    private updateTaskService: UpdateTaskService
  ) {


  }

  initForm() {
    this.taskForm = this.fb.group({
      title: [''],
      completed: [false],
      description: ['']
    })
  }

  setFormValue() {
    if (this.data.task) {
      this.taskForm.patchValue({
        title: this.data.task.title,
        completed: this.data.task.completed,
        description: this.data.task.description
      })
    }
  }

  addTask(task: any) {

    this.todoDataService.addTask(task).subscribe(res => {
      this.updateTaskService.assingTaskValue(res);
      this.notificationService.success('تسک شما با موفقیت اضافه شد');
    })

  }

  updatTask(id: number, task: any) {

    this.todoDataService.updateTaskById(id, task).subscribe((res: any) => {
      this.dialogRef.close(res);
    });
  }

  submit(): void {

    const values = this.taskForm.value;

    if (this.data.task != null) {
      this.updatTask(this.data.task.id, values);
    }
    else {

      if (this.taskForm.value.title) {

        this.addTask(this.taskForm.value);
        this.dialogRef.close();
      } else {
        this.notificationService.error('لطفا نام تسک را وارد نمایید')
      }

    }

  }
}
