import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { NotificationService } from '../shared/services/notificaton.service';
import { Task, TodoDataService } from '../shared/services/todo-data.service';
import { UpdateTaskService } from '../shared/services/update-task.service';
import { ImplementTaskDialogComponent } from './implement-task-dialog/implement-task-dialog.component';

@Component({
  selector: 'app-task-management',
  templateUrl: './task-management.component.html',
  styleUrls: ['./task-management.component.scss']
})
export class TaskManagementComponent implements OnInit {

  tasks: Task[] = [];
  title: FormControl = new FormControl('', Validators.required);

  constructor(
    private todoDataService: TodoDataService,
    private notificationService: NotificationService,
    public dialog: MatDialog,
    private updateTaskService: UpdateTaskService) {

  }

  ngOnInit(): void {
    this.getAllTasks()
  }

  getAllTasks() {
    this.todoDataService.getAllTasks().subscribe((res: any) => {
      this.tasks = res;
    })
  }

  handleDeleteTask(id: number) {

    this.todoDataService.deleteTaskById(id).subscribe((res: any) => {
      this.tasks = res;
      this.notificationService.success('نسک با موفقیت حذف شد')
    })

  }

  handleImplementTask(task: any): void {
    const dialogRef = this.dialog.open(ImplementTaskDialogComponent, {
      width: '500px',
      data: { task },
    });

    dialogRef.afterClosed().subscribe(result => {

      if (!result) {
        this.updateTaskService.task.subscribe(res => {
          this.tasks = res;
        })
      } else {
        task = result;
        this.notificationService.success('تسک به روز رسانی شد.')

      }

    });
  }


}
